#!/bin/bash

function start_routing(){
	while true; do
		wg show wg0 | grep allow | awk '{print $3}' | cut -d "/" -f1 | while read ip; do
			ping -c1 $ip &
		done; wait
		sleep 1
	done
}

function stop_routing(){
	ps aux | grep routing.sh | awk '{print $2}' | while read process; do
		kill $process
	done
}

if [ "$#" -eq 1 ]; then
	echo $1
	if [ "$1" == "start" ]; then
		start_routing
	elif [ "$1" == "stop" ]; then
		stop_routing
	fi

fi
