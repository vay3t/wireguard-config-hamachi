# wireguard-config-hamachi
Wireguard config like a Hamachi

## Install server process
```bash
sudo su
apt install wireguard -y
cd /etc/wireguard/
umask 077; wg genkey | tee privatekey | wg pubkey > publickey
wget https://gitlab.com/vay3t/wireguard-config-hamachi/-/raw/main/wg0.conf
wget https://gitlab.com/vay3t/wireguard-config-hamachi/-/raw/main/routing.sh
# add priv/pub key in config
systemctl enable wg-quick@wg0
```

### Start/Stop wireguard server
```bash
sudo su
systemctl start wg-quick@wg0
systemctl stop wg-quick@wg0
```

## Install client process
```bash
sudo su
apt install wireguard -y
cd /etc/wireguard/
umask 077; wg genkey | tee privatekey | wg pubkey > publickey
wget https://gitlab.com/vay3t/wireguard-config-hamachi/-/raw/main/client-wg0.conf -O hs0.conf
# add priv/pub key in config
wg-quick up hs0
```

### Start/Stop wireguard client
```bash
sudo su
# wg-quick up hs0 # /etc/wireguard/hs0.conf
wg-quick up <basename config client>
wg-quick stop <basename config client>
```
